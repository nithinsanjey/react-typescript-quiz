import React, { useEffect, useState } from "react";
//components
import QuestionCard from "./QuestionCard";
//types
import {
  Difficulty,
  fetchQuizCategories,
  fetchQuizQuestions,
  QuestionState,
} from "./api";
//styles
import { GlobalStyle, Wrapper } from "./App.styles";

export type AnswerObject = {
  question: string;
  answer: string;
  correct: boolean;
  correctAnswer: string;
};

type QuizCategory = {
  id: number;
  name: string;
};

const TOTAL_QUESTION: number = 10;

function App() {
  const [loading, setLoading] = useState(false);
  const [questions, setQuestions] = useState<QuestionState[]>([]);
  const [number, setNumber] = useState(0);
  const [userAnswers, setUserAnswers] = useState<AnswerObject[]>([]);
  const [score, setScore] = useState(0);
  const [gameOver, setGameOver] = useState(true);
  const [quizCategory, setQuizCategory] = useState("0");
  const [quizCategories, setQuizCategories] = useState<QuizCategory[]>([]);

  //logic to fetch category here

  useEffect(() => {
    setLoading(true);
    fetchQuizCategories().then((json) => setQuizCategories(json));
    setLoading(false);
  }, []);

  const startTrivia = async () => {
    setLoading(true);
    setGameOver(false);
    setScore(0);

    console.log("quizCategory" + quizCategory);
    const newQuestions = await fetchQuizQuestions(
      TOTAL_QUESTION,
      Difficulty.EASY,
      quizCategory
    );

    setQuestions(newQuestions);
    setUserAnswers([]);
    setNumber(0);
    setLoading(false);
  };

  const checkAnswer = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (!gameOver) {
      const answer = e.currentTarget.value;
      const correct = questions[number].correct_answer === answer;

      if (correct) setScore((prev) => prev + 1);

      //save answer
      const answerObject = {
        question: questions[number].question,
        answer,
        correct,
        correctAnswer: questions[number].correct_answer,
      };
      console.log(questions[number].correct_answer);
      setUserAnswers((prev) => [...prev, answerObject]);
    }
  };

  const nextQuestion = () => {
    //display next question if it is not the last question
    if (number + 1 === TOTAL_QUESTION) setGameOver(true);
    else {
      setNumber((prev) => prev + 1);
    }
  };

  console.log(questions);

  return (
    <>
      <GlobalStyle />
      <Wrapper>
        <h1>React Quiz</h1>
        {gameOver || userAnswers.length === TOTAL_QUESTION ? (
          <>
            {!loading && (
              <select onChange={(e) => setQuizCategory(e.currentTarget.value)}>
                <option value={0}>Any</option>
                {quizCategories.map((category) => (
                  <option key={category.id} value={category.id}>
                    {category.name}
                  </option>
                ))}
              </select>
            )}
            <button className="start" onClick={startTrivia}>
              Start
            </button>
          </>
        ) : null}
        {loading && <p>Loading....</p>}
        {!gameOver ? <p>Score: {score}</p> : null}
        {!gameOver && !loading && (
          <QuestionCard
            questionNr={number + 1}
            totalQuestions={TOTAL_QUESTION}
            question={questions[number].question}
            answers={questions[number].answers}
            userAnswer={userAnswers ? userAnswers[number] : undefined}
            callback={checkAnswer}
          />
        )}

        {userAnswers[number] &&
          !gameOver &&
          !loading &&
          number !== TOTAL_QUESTION - 1 && (
            <button className="next" onClick={nextQuestion}>
              Next question
            </button>
          )}
      </Wrapper>
    </>
  );
}

export default App;
